using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMusicArea : MonoBehaviour
{
    // 1 forma
    //public AudioManager manager;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //manager.PlayEnemyMusic();

        // 2 forma
        //FindObjectOfType<AudioManager>().PlayEnemyMusic();

        // 3 forma: singleton
        AudioManager.instance.PlayEnemyMusic();
        //AudioManager.PlayEnemyMusicStatic();

    }
}
