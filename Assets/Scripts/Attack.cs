using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.K))
        {
            Debug.DrawRay(transform.position + Vector3.up, Vector2.right, Color.green, 2);
            RaycastHit2D hit = Physics2D.Raycast(transform.position + Vector3.up, Vector2.right);


            //if (hit.collider != null)
            //{
            //    if (hit.collider.CompareTag("Enemigo"))
            //    {
            //        Destroy(hit.collider.gameObject);
            //    }
            //}


            if ((hit.collider != null) && (hit.collider.CompareTag("Enemigo")))
            {
                Destroy(hit.collider.gameObject);
            }
        }
    }
}
