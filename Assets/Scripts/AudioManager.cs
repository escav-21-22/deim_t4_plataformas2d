using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    /* Singleton */
    public static AudioManager instance;

    public AudioSource backgroundMusicAS;
    public AudioSource enemyMusicAS;

    public AudioClip enemyMusic;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        } else if (instance != this)
        {
            Destroy(this);
        }
    }

    public void PlayEnemyMusic()
    {
        //backgroundMusicAS.clip = enemyMusic;
        //backgroundMusicAS.Play();

        enemyMusicAS.Play();
    }

    public static void PlayEnemyMusicStatic()
    {
        instance.backgroundMusicAS.clip = instance.enemyMusic;
    }
}
